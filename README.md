# README #

### Simple Weather Monitor ###

Measures temperature, humidity and sunlight; creates audio alert if rain seems likely. Automatically prints updates to screen every minute.

### Setup ###

* requires STM32 Discovery board, humidity sensor, pyrometer and piezo speaker
* need to have installed minicom, gcc and ARM cross compiler

* program board by going into project directory and using command "make program"
* run project by entering minicom
* minicom commands "weather" and "noweather" to start/stop monitoring

### Credits ###

* Weather monitoring written by: Emily Adams
* Minicom interface, makefile and main.c code provided by Conestoga College, course code: SENG8005 (Bare Metal Programming), Professor: Robert Elder