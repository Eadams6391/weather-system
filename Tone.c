/*
Programmer: Emily Adams
Date: September 9 2016

Description: This file contains very basic
*/

#include <stdio.h>
#include <stdint.h>
#include "stm32f3_discovery.h"
#include "common.h"
#include "stm32f303xc.h"

#include "Timer.h"

#define PWMPERIOD  4


void CTone()
{

    pwm(PWMPERIOD);

    return;
}

void StopTone()
{
    end_pwm();
    GPIOC->BSRRL &= ~GPIO_BSRR_BS_8;//set PC8 to '0'
    GPIOC->BSRRH |= GPIO_BSRR_BS_8;

    return;
}


