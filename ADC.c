/*
Programmer: Karan Bidja
Modified by: Emily Adams
Date: September 9 2016

Description: This file contains various functions the use one of the ADC channels to
         read and calculate a value for a metorlogical sensor. Measurements taken are
         for temperature (in celcius), relative humidity, and solar intensity (as a percentage).
*/
#include <stdio.h>
#include <stdint.h>
#include "stm32f3xx_hal.h"
#include "common.h"
#include "stm32f3xx_hal_adc.h"

// ADC handle declaration
ADC_HandleTypeDef hadc_temp;
ADC_HandleTypeDef hadc_rh;
ADC_HandleTypeDef hadc_light;

void setupADCs()
{
   GPIO_InitTypeDef GPIO_InitStruct;
/* Enable the ADC interface clock using __ADC_CLK_ENABLE() */
   __ADC12_CLK_ENABLE();
   __HAL_RCC_ADC12_CONFIG(RCC_ADC12PLLCLK_DIV1); /* ADC pins configuration*/
   __ADC34_CLK_ENABLE();
   __HAL_RCC_ADC34_CONFIG(RCC_ADC34PLLCLK_DIV1);
/*Enable the clock for the ADC GPIOs */
   __GPIOA_CLK_ENABLE();
   __GPIOB_CLK_ENABLE();
/* Configure these ADC pins in analog mode using HAL_GPIO_Init() */

//pin PA4 is used for relative humidity measurement
   GPIO_InitStruct.Pin = (GPIO_PIN_4);
   GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
   GPIO_InitStruct.Pull = GPIO_NOPULL;
   GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
   GPIO_InitStruct.Alternate = 0;
   HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

//pin PB1 is used for solar intensity
   GPIO_InitStruct.Pin = (GPIO_PIN_1);
   GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
   GPIO_InitStruct.Pull = GPIO_NOPULL;
   GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
   GPIO_InitStruct.Alternate = 0;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

/* Initialize ADC for temperature sensor*/
   hadc_temp.Instance = ADC1;
   hadc_temp.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV1;
   hadc_temp.Init.Resolution = ADC_RESOLUTION12b;
   hadc_temp.Init.DataAlign = ADC_DATAALIGN_RIGHT;
   hadc_temp.Init.ScanConvMode = ADC_SCAN_DISABLE;
   hadc_temp.Init.EOCSelection = EOC_SINGLE_CONV;
   hadc_temp.Init.LowPowerAutoWait = DISABLE;
   hadc_temp.Init.ContinuousConvMode = DISABLE;
   hadc_temp.Init.NbrOfConversion = 1;
   hadc_temp.Init.DiscontinuousConvMode = DISABLE;
   hadc_temp.Init.NbrOfDiscConversion = 0;
   hadc_temp.Init.ExternalTrigConv = ADC_SOFTWARE_START;
   hadc_temp.Init.ExternalTrigConvEdge = 0;
   hadc_temp.Init.DMAContinuousRequests = DISABLE;
   hadc_temp.Init.Overrun = OVR_DATA_OVERWRITTEN;
   HAL_StatusTypeDef rc;
   rc = HAL_ADC_Init(&hadc_temp);
   if(rc != HAL_OK) {
      printf("ADC1 initialization failed with rc=%u\n",rc);
   }

/* Initialize ADC for relative humidity*/
   hadc_rh.Instance = ADC2;
   hadc_rh.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV1;
   hadc_rh.Init.Resolution = ADC_RESOLUTION12b;
   hadc_rh.Init.DataAlign = ADC_DATAALIGN_RIGHT;
   hadc_rh.Init.ScanConvMode = ADC_SCAN_DISABLE;
   hadc_rh.Init.EOCSelection = EOC_SINGLE_CONV;
   hadc_rh.Init.LowPowerAutoWait = DISABLE;
   hadc_rh.Init.ContinuousConvMode = DISABLE;
   hadc_rh.Init.NbrOfConversion = 1;
   hadc_rh.Init.DiscontinuousConvMode = DISABLE;
   hadc_rh.Init.NbrOfDiscConversion = 0;
   hadc_rh.Init.ExternalTrigConv = ADC_SOFTWARE_START;
   hadc_rh.Init.ExternalTrigConvEdge = 0;
   hadc_rh.Init.DMAContinuousRequests = DISABLE;
   hadc_rh.Init.Overrun = OVR_DATA_OVERWRITTEN;

   rc = HAL_ADC_Init(&hadc_rh);
   if(rc != HAL_OK) {
      printf("ADC2 initialization failed with rc=%u\n",rc);
   }

/* Initialize ADC for solar intensity*/
   hadc_light.Instance = ADC3;
   hadc_light.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV1;
   hadc_light.Init.Resolution = ADC_RESOLUTION12b;
   hadc_light.Init.DataAlign = ADC_DATAALIGN_RIGHT;
   hadc_light.Init.ScanConvMode = ADC_SCAN_DISABLE;
   hadc_light.Init.EOCSelection = EOC_SINGLE_CONV;
   hadc_light.Init.LowPowerAutoWait = DISABLE;
   hadc_light.Init.ContinuousConvMode = DISABLE;
   hadc_light.Init.NbrOfConversion = 1;
   hadc_light.Init.DiscontinuousConvMode = DISABLE;
   hadc_light.Init.NbrOfDiscConversion = 0;
   hadc_light.Init.ExternalTrigConv = ADC_SOFTWARE_START;
   hadc_light.Init.ExternalTrigConvEdge = 0;
   hadc_light.Init.DMAContinuousRequests = DISABLE;
   hadc_light.Init.Overrun = OVR_DATA_OVERWRITTEN;

   rc = HAL_ADC_Init(&hadc_light);
   if(rc != HAL_OK) {
      printf("ADC3 initialization failed with rc=%u\n",rc);
   }
   return;
}

int32_t readTemp()
{ 
  HAL_StatusTypeDef rc;
  ADC_ChannelConfTypeDef config;
  int channel = ADC_CHANNEL_TEMPSENSOR;
  uint32_t val;
  int32_t volt, temp;

  /* Configure the selected channel */
  config.Channel = channel;
  config.Rank = 1;  /* Rank needs to be 1, otherwise no conversion is done */
  config.SamplingTime = ADC_SAMPLETIME_181CYCLES_5;
  config.SingleDiff = ADC_SINGLE_ENDED;
  config.OffsetNumber = ADC_OFFSET_NONE;
  config.Offset = 0;
  rc = HAL_ADC_ConfigChannel(&hadc_temp,&config);
  if(rc != HAL_OK) {
    printf("ADC channel configure failed with rc=%u\n",(unsigned)rc);
    return -1;
  }

  /* Start the ADC peripheral */
  rc = HAL_ADC_Start(&hadc_temp);
  if(rc != HAL_OK) {
    printf("ADC start failed with rc=%u\n",(unsigned)rc);
    return -1;
  }

  /* Wait for end of conversion */
  rc = HAL_ADC_PollForConversion(&hadc_temp, 100);
  if(rc != HAL_OK) {
    printf("ADC poll for conversion failed with rc=%u\n",(unsigned)rc);
    return -1;
  }
  /* Read the ADC converted values */
  val = HAL_ADC_GetValue(&hadc_temp);
  volt = val*33000/4095;//mutiply by 10,000 because stm does not handle float conversions well
  temp = (14300 - volt)/43 + 250000;

  /* Stop the ADC peripheral */
  rc = HAL_ADC_Stop(&hadc_temp);
  if(rc != HAL_OK) {
    printf("ADC stop failed with rc=%u\n",(unsigned)rc);
    return -1;
  }
  return temp;
}

int32_t readHumid()
{
  HAL_StatusTypeDef rc;
  ADC_ChannelConfTypeDef config;
  int channel = 1;

  uint32_t val;
  int32_t rh;

  /* Configure the selected channel */
  config.Channel = channel;
  config.Rank = 1;  /* Rank needs to be 1, otherwise no conversion is done */
  config.SamplingTime = ADC_SAMPLETIME_181CYCLES_5;
  config.SingleDiff = ADC_SINGLE_ENDED;
  config.OffsetNumber = ADC_OFFSET_NONE;
  config.Offset = 0;
  rc = HAL_ADC_ConfigChannel(&hadc_rh,&config);
  if(rc != HAL_OK) {
    printf("ADC channel configure failed with rc=%u\n",(unsigned)rc);
    return -1;
  }

  /* Start the ADC peripheral */
  rc = HAL_ADC_Start(&hadc_rh);
  if(rc != HAL_OK) {
    printf("ADC start failed with rc=%u\n",(unsigned)rc);
    return -1;
  }

  /* Wait for end of conversion */
  rc = HAL_ADC_PollForConversion(&hadc_rh, 100);
  if(rc != HAL_OK) {
    printf("ADC poll for conversion failed with rc=%u\n",(unsigned)rc);
    return -1;
  }
  /* Read the ADC converted values */
  val = HAL_ADC_GetValue(&hadc_rh);
  rh = val * 100000/4095;

  /* Stop the ADC peripheral */
  rc = HAL_ADC_Stop(&hadc_rh);
  if(rc != HAL_OK) {
    printf("ADC stop failed with rc=%u\n",(unsigned)rc);
    return -1;
  }
  return rh;
}

int32_t readSolar()
{
  HAL_StatusTypeDef rc;
  ADC_ChannelConfTypeDef config;
  int channel = 1;

  uint32_t val;
  int32_t light;

  /* Configure the selected channel */
  config.Channel = channel;
  config.Rank = 1;  /* Rank needs to be 1, otherwise no conversion is done */
  config.SamplingTime = ADC_SAMPLETIME_181CYCLES_5;
  config.SingleDiff = ADC_SINGLE_ENDED;
  config.OffsetNumber = ADC_OFFSET_NONE;
  config.Offset = 0;
  rc = HAL_ADC_ConfigChannel(&hadc_light,&config);
  if(rc != HAL_OK) {
    printf("ADC channel configure failed with rc=%u\n",(unsigned)rc);
    return -1;
  }

  /* Start the ADC peripheral */
  rc = HAL_ADC_Start(&hadc_light);
  if(rc != HAL_OK) {
    printf("ADC start failed with rc=%u\n",(unsigned)rc);
    return -1;
  }

  /* Wait for end of conversion */
  rc = HAL_ADC_PollForConversion(&hadc_light, 100);
  if(rc != HAL_OK) {
    printf("ADC poll for conversion failed with rc=%u\n",(unsigned)rc);
    return -1;
  }
  /* Read the ADC converted values */
  val = HAL_ADC_GetValue(&hadc_light);
  light = val * 100000/4095;//calculating as percent

  /* Stop the ADC peripheral */
  rc = HAL_ADC_Stop(&hadc_light);
  if(rc != HAL_OK) {
    printf("ADC stop failed with rc=%u\n",(unsigned)rc);
    return -1;
  }
  return light;
}


