#ifndef TIMER_H_   /* Include guard */
#define TIMER_H_

void delay_us(int32_t delay);

void delay_ms(int32_t delay);

void setInterval_ms(int32_t period);

void pwm(uint32_t period);

void dutyadjust(int32_t duty);

void end_pwm(void);

#endif // TIMER_H_
