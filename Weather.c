/*
Programmer: Emily Adams
Date: September 9 2016

Description: The functions here consolidate and display the data from the various 
      sensor program files. Once activated, the program updates the data every
      minute, and may activate a buzzer for 2 seconds if rain seems likely.

*/

#include <stdio.h>
#include <stdint.h>
#include "stm32f3_discovery.h"
#include "common.h"
#include "stm32f303xc.h"

#include "Weather.h"
#include "Timer.h"
//#include "Tone.h"
#include "ADC.h"

int newWeather = 0;
int32_t temp = 0;
int32_t humid = 0;
int32_t solar = 0;

void startWeatherMonitor(int mode)
{
    if(mode != CMD_INTERACTIVE) {
      return;
    }
     setupADCs();
     readTemp();//first read always off so junk it here
     setInterval_ms(2000); //interrupt every 2 s
     return;
}

ADD_CMD("weather", startWeatherMonitor,"                get weather updates each minute");

void stopWeatherMonitor(int mode)
{
    if(mode != CMD_INTERACTIVE) {
      return;
    }
    
    NVIC->ISER[0] &= ~(1 << 30); // disable TIM4 update interrupt in NVIC
    TIM4->CR1 &= ~(TIM_CR1_CEN);// disable timer
    TIM4->DIER &= ~(TIM_DIER_UIE);// disable timer4 update interrupts
    TIM4->CR1 |= TIM_CR1_UDIS; //set update disable bit
    RCC->APB1ENR &= ~RCC_APB1ENR_TIM4EN;//disable timer4
    return;
}

ADD_CMD("noweather", stopWeatherMonitor,"                stop weather updates");

void displayWeather()
{
     printf("Current Weather:\n");      
     printf("Temperature: %u.%u degrees Celcius\n",(unsigned)(temp/10000), (unsigned)(temp%10000));
     printf("Relative Humidity: %u.%u%%\n",(unsigned)(humid/1000), (unsigned)(humid%1000));
     if(solar < 10000) //if sunlight is dim (less than 10% of max reading)
     {
        printf("Cloudy\n");
     }
     else 
     {
        printf("Sunny\n");
     }
     return;
}

void TIM4_IRQHandler (void)
{ 
    if(TIM4->SR & TIM_SR_UIF) // if UIF flag is set
    {
       TIM4->SR &= ~TIM_SR_UIF; // clear UIF flag
       static int count = 0;
       static int alarmOn = 0;


       if( count >= 29 )
       {
          temp = readTemp();
          humid = readHumid();
          solar = readSolar();
          if(humid > 70000 && solar < 10000) //if humidity is high and sunlight is dim
          {
             pwm(4);//send out a square wave to sound the buzzer 
             alarmOn = 1;//indicate alarm is on
          }

          newWeather = 1;
  
          count = 0;
       }
       else if( alarmOn == 1 )//after one 2 seconds interval, if alarm was on
       {
          end_pwm();//stop the alarm
          GPIOC->BSRRL &= ~GPIO_BSRR_BS_8;
          GPIOC->BSRRH |= GPIO_BSRR_BS_8;//set PC8 to '0'
          alarmOn = 0;
          count++;
       }
       else
       {
          count++;
       }
   }
   return;
}

