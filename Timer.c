/*
Programmer: Emily Adams
Date: September 9 2016

Description: This file contains various timer functions to create delays, regular interupt
      events, and send a pulse width modulated signal. Each functionality uses a different
      timer, allowing different modes to run "simultaniously"

Code referenced:http://en.radzio.dxp.pl/stm32vldiscovery/lesson3,blinking,with,timer.html
*/

#include <stdio.h>
#include <stdint.h>
#include "stm32f3_discovery.h"
#include "stm32f303xc.h"
#include "core_cm4.h"

#include "Timer.h"

void delay_us(int32_t delay)
{
    RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;//enable timer1
    
    TIM1->PSC = 71; //set prescale to 72 (prescale multiplier is PSC+1)
    TIM1->ARR = delay; //load the timer to count <delay> number of ticks
    TIM1->CR1 |= TIM_CR1_CEN;// enable timer
    while((TIM1->SR & TIM_SR_UIF)==0); //wait for update event flag
    TIM1->SR &= ~TIM_SR_UIF;//clear flag
    TIM1->CR1 &= ~TIM_CR1_CEN;// disable timer
    return;
}

void delay_ms(int32_t delay)
{
    RCC->APB2ENR |= RCC_APB2ENR_TIM1EN;//enable timer1
    
    TIM1->PSC = 71999; //set prescale to 72000 (prescale multiplier is PSC+1)
    TIM1->ARR = delay; //load the timer to count <delay> number of ticks
    TIM1->CR1 |= TIM_CR1_CEN;// enable timer
    while((TIM1->SR & TIM_SR_UIF) == 0); //wait for update event flag
    TIM1->SR &= ~(TIM_SR_UIF);//clear flag
    TIM1->CR1 &= ~(TIM_CR1_CEN);// disable timer
    return;
}

void setInterval_ms(int32_t period)
{
    RCC->APB1ENR |= RCC_APB1ENR_TIM4EN;//enable timer4

    TIM4->CR1 &= ~TIM_CR1_DIR; //set direction to count up
    
    TIM4->PSC = 35999; //set prescale to 36000 (prescale multiplier is PSC+1)
//The clock speed is 72MHz, so a prescale of 72000 would let the timer count in milliseconds
//but the prescaler doesn't go that high for this timer, thus we count in intervals of 0.5 ms instead
    TIM4->ARR = period*2; //load the timer to count <period> number of ticks times 2 
    TIM4->CR1 &= ~TIM_CR1_UDIS; //clear update disable bit
    TIM4->DIER |= TIM_DIER_UIE;// enable timer4 update interrupts
    TIM4->CR1 |= TIM_CR1_CEN;// enable timer

    NVIC->ISER[0] |= 1 << 30; // enable TIM4 update interrupt in NVIC
    return;
}

void pwm(uint32_t period)
{
    RCC->AHBENR |= RCC_AHBENR_GPIOCEN; // Enable GPIOC clock
    RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; // Enable Timer 3 clock
    // PC8 configuration
    GPIOC->MODER |= 2 << (8*2); // Alternate function mode
    GPIOC->OTYPER |= 0 << 8; // Output push-pull (reset state)
    GPIOC->OSPEEDR |= 0 << (8*2); // 2 MHz High speed
    GPIOC->AFR[1] |= 2 << ((8-8)*4); // Select AF2 for PC8: TIM3_CH3
    TIM3->PSC = 35999; // Set prescaler to 36000 (PSC + 1) for 0.5 ms interval
    TIM3->ARR = period*2; // Set auto reload value to <period> ms
    TIM3->CCR3 = period; // Start PWM duty for channel 3 with 50% duty cycle
    TIM3->CCMR2 |= TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1; // PWM mode 1 on channel 3
    TIM3->CCER |= TIM_CCER_CC3E; // Enable compare on channel 3
    TIM3->CR1 |= TIM_CR1_CEN; // Enable timer
    return;
}

void dutyadjust(int32_t duty)
{
     if(duty < 0 && TIM3->CCR3 > 0)
     {
        TIM3->CCR3--;
     }
     else if(duty > 0 && TIM3->CCR3 < TIM3->ARR)
     {
        TIM3->CCR3++;
     }
     return;
}
//function below is just a quick debug function
//should not appear in final code
void newduty(uint32_t duty)
{
    TIM3->CCR3 = duty; 
    return;
}
void end_pwm()
{
    TIM3->CCER &= ~TIM_CCER_CC3E; // Disable compare on channel 3
    TIM3->CR1 &= ~TIM_CR1_CEN; // Disable timer
    return;
}

