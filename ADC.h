#ifndef ADC_H_   /* Include guard */
#define ADC_H_

void setupADCs(void);

int32_t readTemp(void);

int32_t readHumid(void);

int32_t readSolar(void);

#endif // TONE_H_
